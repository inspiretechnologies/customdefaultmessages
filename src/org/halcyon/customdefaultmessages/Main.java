package org.halcyon.customdefaultmessages;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import org.halcyon.customdefaultmessages.EventListener; // Import event handler class
import org.halcyon.customdefaultmessages.CommandCDM; // Import command handler class
import org.halcyon.customdefaultmessages.Lag;

public class Main extends JavaPlugin {

	FileConfiguration config = getConfig();
	
	
	@Override
    public void onEnable() {
		
		getServer().getPluginManager().registerEvents(new EventListener(this), this);
		getCommand("cdm").setExecutor(new CommandCDM(this)); //Register the command
		
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Lag(), 100L, 1L);
		
        config.addDefault("block-default-join-message", false);
        config.addDefault("custom-join-message", "Player joined the server");
        config.addDefault("on-join-chat", false);
        config.addDefault("on-join-chat-message", "Welcome {name} to the server!");
        config.options().copyDefaults(true);
        saveConfig();
	}
    
	@Override
    public void onDisable() {
		
	}
	
	
	public void consoleMessage(String outputMessage) {
    	Bukkit.getLogger().info("[CustomDefaultMessages] " + outputMessage);
    }
	
}

