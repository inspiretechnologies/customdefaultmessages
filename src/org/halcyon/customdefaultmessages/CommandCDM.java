package org.halcyon.customdefaultmessages;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import org.halcyon.customdefaultmessages.Lag;


public class CommandCDM implements CommandExecutor
{

	private Main plugin;
    public CommandCDM(Main plugin) {
        this.plugin = plugin;
    }
	
	@Override 
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
    	plugin.consoleMessage("Command for CustomDefaultMessages received");
        if (command.getName().equalsIgnoreCase("cdm")) {
        	//sender.sendMessage("Not implemented.");
        	plugin.consoleMessage("Command was cdm");
        	
        	if (args.length > 0) 
        	{  
        		if (args[0].equalsIgnoreCase("reload")) {
        			plugin.consoleMessage("Got reload command from " + sender.getName());
        			plugin.reloadConfig();
        			sender.sendMessage("Configuration files reloaded");
        		} else if (args[0].equalsIgnoreCase("save")) {
        			plugin.consoleMessage("Got reload command from " + sender.getName());
        			plugin.saveConfig();
        			sender.sendMessage("Configuration files updated with live changes");
        		}
        	} else {
        		sender.sendMessage("Missing arguments");
        	}
        }
        return true;
	}
	
	
	//public void consoleMessage(String outputMessage) {
    //	Bukkit.getLogger().info("[CustomDefaultMessages] " + outputMessage);
    //}
	
}
