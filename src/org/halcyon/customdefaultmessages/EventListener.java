package org.halcyon.customdefaultmessages;

import java.text.DecimalFormat;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import org.halcyon.customdefaultmessages.Lag;

import org.halcyon.customdefaultmessages.Main;


public class EventListener implements Listener {

	private Main plugin;
    public EventListener(Main plugin) {
        this.plugin = plugin;
    }
    
    
    @EventHandler
    public void onPlayerChangeWorld(PlayerChangedWorldEvent event) {
		//event.setJoinMessage(null);
    	Player p = event.getPlayer();
    	String playerName = p.getName();
    	String playerWorld = p.getWorld().getName();
    	String playerPrevWorld = event.getFrom().getName();
    	
		plugin.consoleMessage("Player " + playerName + " changed from " + playerPrevWorld + " to " + playerWorld);
	}
    
    
	
	
	@EventHandler (priority = EventPriority.HIGHEST, ignoreCancelled = true)
     public void onPlayerJoin(PlayerJoinEvent event) {
		String theMessage="";
		String logMessage="";
		
		if (plugin.config.getBoolean("on-join-chat")) {
			plugin.consoleMessage("inside on-join-chat function");
			Player p = event.getPlayer();
			logMessage = "the player that joined is " + p.getName();
			plugin.consoleMessage(logMessage);
			theMessage = plugin.getConfig().getString("on-join-chat-message");
			theMessage = StringUtils.stripStart(theMessage, "\\");
			plugin.consoleMessage(formatConfigMessage(p, theMessage));
			Bukkit.broadcastMessage(formatConfigMessage(p, theMessage));
		}
		else {
			// Nothing
		}
		
		if (plugin.config.getBoolean("block-default-join-message")) {
			plugin.consoleMessage("inside block-default-join-message function");
			Player p = event.getPlayer();
			theMessage = plugin.getConfig().getString("custom-join-message");
			theMessage = StringUtils.stripStart(theMessage, "\\");
			event.setJoinMessage(null);
			event.setJoinMessage(formatConfigMessage(p, theMessage));
		}
		else {
			// Nothing
		}
	}
	
	
	public String formatConfigMessage(Player p, String theMessage) {
		String lastConnected="";
		String lastConnectedDate="";
		String lastConnectedTime="";
		theMessage = theMessage.replace("{player}", p.getName());
		theMessage = theMessage.replace("{world}", p.getWorld().getName());
		String pX = String.valueOf(p.getLocation().getBlockX());
		String pY = String.valueOf(p.getLocation().getBlockY());
		String pZ = String.valueOf(p.getLocation().getBlockZ());
		theMessage = theMessage.replace("{x}", pX);
		theMessage = theMessage.replace("{y}", pY);
		theMessage = theMessage.replace("{z}", pZ);
		theMessage = theMessage.replace("{xyz}", "[" + pX + "," + pY + "," + pZ + "]");
		if (p.hasPlayedBefore()) {
			lastConnected = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date (p.getLastPlayed()));
			lastConnectedDate = new java.text.SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date (p.getLastPlayed()));
			lastConnectedTime = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date (p.getLastPlayed()));
		}
		else { lastConnected = "never"; }
		theMessage = theMessage.replace("{lastPlayed}", lastConnected);
		theMessage = theMessage.replace("{lastPlayedDate}", lastConnectedDate);
		theMessage = theMessage.replace("{lastPlayedTime}", lastConnectedTime);
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
        String currentTPS = decimalFormat.format(Lag.getTPS());
        theMessage = theMessage.replace("{TPS}", currentTPS);
        return theMessage;
	}
}
